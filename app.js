    
		

const app = new Vue({
  el: '#app',
  data: {
    products: null,
		currentCart: null,
			delivery:"",
				Fdelivery:null
  },
  mounted () {
		const vm = this
		vm.fetchData()
	},
	computed: {
		currentCartId: function () {
			const urlParams = new URLSearchParams(window.location.search)
			return urlParams.get('id')
		},
		currentCartProducts: function () {
			const vm = this
			if (!vm.currentCartId) return
			if (!vm.currentCart) return
			if (!vm.currentCart.products) return
			if (!vm.products) return

			let products = []
			vm.currentCart.products.forEach(cartProduct => {
				let searchedProductById = vm.products.find(el => el.id == cartProduct.productId)
				products.push(searchedProductById)
			})
			return products
		}
	},
	methods: {
		formatPrice(value) {
        	let val = (value/1).toFixed(2).replace('.', ',') +" zł"
        	return val
    	},
		calculatePartialSum() {
			const vm = this
			let sum=0
			if (!vm.currentCartId) return
			if (!vm.currentCart) return
			if (!vm.currentCart.products) return
			if (!vm.products) return

			let products = []
			vm.currentCart.products.forEach(cartProduct => {
				let searchedProductById = vm.products.find(el => el.id == cartProduct.productId)
				sum+=searchedProductById.price*cartProduct.quantity
			})
			return sum
		},
		calculateFullSum() {
			const vm = this
			let sum=0
			if (!vm.currentCartId) return
			if (!vm.currentCart) return
			if (!vm.currentCart.products) return
			if (!vm.products) return

			let products = []
			vm.currentCart.products.forEach(cartProduct => {
				let searchedProductById = vm.products.find(el => el.id == cartProduct.productId)
				sum+=searchedProductById.price*cartProduct.quantity+vm.Fdelivery
			})
			return sum
		},

		selectDeliveryMethod(type)
		{
			const vm = this
			if(type == "inpost"){
				vm.delivery= "10,99 zł"
				vm.Fdelivery= 10.99}
			if(type == "dpd"){
				vm.delivery= "18,00 zł"
				vm.Fdelivery= 18.00}
			if(type == "dpd-delivery_payment"){
				vm.delivery= "22,00 zł"		
				vm.Fdelivery= 22.00}
			vm.calculateFullSum()
		},
		fetchData () {
			const vm = this
			vm.getProducts()
			vm.getCart(vm.currentCartId)
		},
		getProducts () {
			const vm = this
			fetch('https://fakestoreapi.com/products/')
				.then(res => res.json())
				.then(res => {
					console.log(res)
					vm.products = res
				})
		},
		getCart (id) {
			const vm = this
			fetch(`https://fakestoreapi.com/carts/${id}`)
				.then(res => res.json())
				.then(res => {
					vm.currentCart = res
				})
		}
	}
})

		
	// 	var object;
  //   fetch('https://fakestoreapi.com/products')
  //           .then(res=>res.json())
  //           .then(json=>console.log(json))
  // fetch('https://fakestoreapi.com/carts/2')
  //   .then(res=>res.json())
  //   .then((json)=>{
  //     cartProducts= json;
  //     console.log(json)
  //     console.log(object)
  //     console.log(json.products)
  //   })

  // fetch('https://fakestoreapi.com/products/')
  //   .then((data)=>{
  //     return data.json();
  //   })
  //   .then((completeData)=>{
  //     let data1="";
  //     completeData.map((values)=>{

  //       for (var i = 0; i < cartProducts.products.length; i++)
  //       {    
  //         if(values.id==cartProducts.products[i].productId){
  //         data1+=` <div class="row py-2">
  //           <div class="col-md-4">
  //             <img style="width: 100%; height: 100%;"src=${values.image} alt="DPD pobranie">
  //           </div>
  //           <div class="col-md-8">
  //             <label style="font-weight:600;" class="form-check-label col-md-6" for="flexRadioDefault1">
  //               ${values.title}
  //             </label>              
  //             <label style="font-weight:600;"class="form-check-label col-md-5" for="flexRadioDefault1">
  //               `+new Intl.NumberFormat('pl-PL', { style: 'currency', currency: 'PLN' }).format(values.price)+
  //             `</label>
  //             <label class="form-check-label col-md-12" for="flexRadioDefault1">
  //               Ilość: ${object.products[0].quantity}
  //             </label>
  //           </div>
  //         </div>`
  //       }
  //     }})
  //     document.getElementById("productsSummary").innerHTML=data1;
  //   }).catch((err)=>{
  //     console.log(err);
  //   })   